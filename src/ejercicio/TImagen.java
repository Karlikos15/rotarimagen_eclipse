package ejercicio;

/**
 * Clase para rotar imagenes bmp 40x40. Los ficheros bmp 40x40 
 * se componen: 54 bytes cabeceras,
 * 40*40*3 = 4800 bytes de información de la imagen.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import java.io.*;

public class TImagen
{
    public static final int ANCHO = 40;
    public static final int ALTO = 40;
    public static final int TAM_CABECERA = 54;
    private byte [] cabecera = new byte[TAM_CABECERA];
    private TPixel [][] imagen = new TPixel[ALTO][ANCHO];

    /**
     * Devuelve cierto si existe el fichero con nombre.
     */
    public static boolean existe(String nombre){
        File fichero = new File (nombre);
        return fichero.exists();
    }

    /**
     * Carga una imagen bmp ANCHOxALTO con nombre dado.
     * Devuelve cierto si en el fichero con nombre hay los bytes adecuados.
     * @param nombre String con nombre de fichero para leer bytes de imagen.
     * @return cierto si tiene los bytes de una imagen bmp ANCHOxALTO.
     */
    public boolean cargarImagen(String nombre) {
        boolean cargaImagen=true;
        try{
            FileInputStream fichero = new FileInputStream(nombre);

            for(int i=0;i<TAM_CABECERA;i++){
                cabecera[i] = (byte)fichero.read();
            }

            for(int j=0;j < ANCHO;j++){
                for(int k=0;k < ALTO;k++){

                    imagen[j][k] = new TPixel((byte)fichero.read(), (byte)fichero.read(), (byte)fichero.read());

                }
            }
        }catch(FileNotFoundException fnfe){
            cargaImagen=false;
            System.out.println("No se encuentra el fichero");
        }catch(IOException ioe){
            cargaImagen=false;
            System.out.println("Fuera de rango");
        }
        return cargaImagen;
    }

    /**
     * Rota la imagen 180 grados.
     */
    public void rotar180() {
        TPixel [][] aux = new TPixel[ALTO][ANCHO];
        for(int j=0;j < ANCHO;j++){
            for(int k=0;k < ALTO; k++){
                aux[j][k]=imagen[39-j][39-k];

            }
        }

        for(int j=0;j < ANCHO; j++){
            for(int k=0;k < ALTO; k++){
                imagen[j][k]=aux[j][k];

            }
        }
    }
    
    /**
     * Rota la imagen 90 grados.
     */
    public void rotar90() {
        TPixel [][] aux = new TPixel[ALTO][ANCHO];
        for(int j=0;j < ANCHO;j++){
            for(int k=0;k < ALTO; k++){
                aux[j][k]=imagen[j][39-k];

            }
        }

        for(int j=0;j < ANCHO; j++){
            for(int k=0;k < ALTO; k++){
                imagen[j][k]=aux[39-k][39-j];

            }
        }
    }

    /**
     * Guarda en el fichero con nombre dado los datos de la imagen.
     * @param nombre String con nombre de fichero para almacenar los datos de la imagen.
     */
    public void volcarImagen(String nombre) {

        try{
            FileOutputStream fichero = new FileOutputStream(nombre);

            fichero.write(cabecera);

            for(int j=0;j < ANCHO; j++){
                for(int k=0; k < ALTO; k++){

                    fichero.write(imagen[j][k].getRed());
                    fichero.write(imagen[j][k].getGreen());
                    fichero.write(imagen[j][k].getBlue());
                }
            }
            fichero.close();
        }catch(FileNotFoundException fnf){

            System.out.println("No se encuentra el archivo");
        }catch(IOException ioe){

            System.out.println("Fuera de rango");
        }

    }

    /**
     * programa principal
     */

    public static void main(String[] args) {

        TImagen imag = new TImagen();

        if (existe("original1.bmp")){
            boolean cargaImagen = imag.cargarImagen("original1.bmp");

            if (cargaImagen) { 
                imag.rotar180();
                imag.volcarImagen("rotado180.bmp");
                imag.rotar90();
                imag.volcarImagen("rotado90.bmp");
            } else {
                System.out.println("El archivo no es imagen bmp 40x40");
            }
        }
        else
            System.out.println("No se encuentra el archivo");

    }

}
