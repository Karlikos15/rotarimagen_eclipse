package ejercicio;


/**
 * Clase para representar un Pixel.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class TPixel
{
    // instance variables - replace the example below with your own
    private byte r , g , b ;

    /**
     * Constructor for objects of class TPixel
     */
    public TPixel(byte nr, byte ng, byte nb)
    {
        
        r = nr ;
        g = ng ;
        b = nb ;
    }

    public byte getRed() {
        return r;
    }
    
    public byte getGreen() {
        return g;
    }
    
    public byte getBlue() {
        return b;
    }
}

